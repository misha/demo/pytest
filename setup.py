from setuptools import setup, find_packages

setup(
    name='fizzbuzz',
    version='1.0.0',
    url='https://gitlab.com/vous/fizzbuzz.git',
    author='Vous',
    author_email='vous@quelquechose',
    description='Réalisation du TP Build + Tests + Intégration continue',
    packages=find_packages(),
)
